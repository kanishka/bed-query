package bedquery

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

import aws.MockAthenaClient

object QueryExecution {

  import MockAthenaClient._

  /// Execute a SQL query in Athena against a BED file with a detected schema.
  /// - projections: the columns to return, some columns may have functions applied to them (the SQL Select clause)
  /// - restrictions: the filter criteria to use (the SQL Where clause)
  /// - table: the table name which was created in AWS Glue catalog with the detected schema
  /// - database: the database where the table resides
  /// - athenaClient: a connected AWS Athena client
  /// - outputFolder: the S3 folder that Athena will place query result intermediate files
  /// Returns: A future, with a sequence of rows, where each row consists of a sequence of string values
  private[bedquery] def executeQuery(projections: ProjectionClause, restrictions: RestrictionClause)(
    table: TableName, database: DatabaseName, athenaClient: MockAthenaClient, outputFolder: S3Location)(
    implicit ec: ExecutionContext): Future[Seq[Seq[String]]] = {

    for {query <- Future.fromTry(buildQuery(database, table, restrictions, projections))
         // TODO: we should automatically create the output folder and delete it once the query has finished
         queryExecId <- Future.fromTry(athenaClient.startQueryExecution(query.value, database.value, outputFolder.value).map(QueryExecutionId(_)))
         _ <- awaitResult(queryExecId)(athenaClient)
         rows <- fetchRows(queryExecId)(athenaClient)
    } yield rows
    // TODO: delete the database upon successful completion of query
  }

  /// Perform some validation of the query parts and construct a SQL query string
  private def buildQuery(dbName: DatabaseName, table: TableName, restrictions: RestrictionClause, projections: ProjectionClause):
    Try[Query] = {

    for {_ <- if (projections.value.nonEmpty) Success(()) else Failure(new IllegalArgumentException("empty projections"))
         _ <- if (restrictions.value.nonEmpty) Success(()) else Failure(new IllegalArgumentException("empty restrictions"))
    } yield Query(s" SELECT ${projections.value} FROM ${dbName.value}.${table.value} WHERE ${restrictions.value} ")
  }

  /// Await completion of executing a query
  private def awaitResult(queryExecId: QueryExecutionId)(athenaClient: MockAthenaClient)(implicit ec: ExecutionContext):
    Future[Unit] = {
    // Note: consider switching to using a Timer or investigating whether AWS async api actually supports non-blocking

    var result: Option[Try[Unit]] = None
    // TODO: take timeout parameter and compute timeRemaining
    var timeRemaining = true
    while (timeRemaining && result.isEmpty) {
      athenaClient.getQueryExecution(queryExecId.value) match {
        case Success(Succeeded) => result = Some(Success(()))
        case Success(Failed) => result = Some(Failure(new Exception("query execution failed")))
        case Success(Cancelled) => result = Some(Failure(new Exception("query execution cancelled")))
        case failure => result = Some(failure.map(_ => ()))
      }
      Thread.sleep(QueryStatusCheckDelayMillis)
    }
    result match {
      case Some(value) => Future.fromTry(value)
      case None => Future.failed(new Exception("Timeout"))
    }
  }

  /// Retrieve the rows from the completed query
  private def fetchRows(queryExecId: QueryExecutionId)(athenaClient: MockAthenaClient)(implicit ec: ExecutionContext):
    Future[Seq[Seq[String]]] = {
    // TODO: use a richer type for rows, with column specific types

    Future.fromTry(
      for {results <- athenaClient.getQueryResultsPaginator(queryExecId.value)}
        yield {
          results.foldLeft(Seq[Seq[String]]())((soFar, nextResult) =>
            // TODO: fetch and store headers
            soFar ++ nextResult.rows)
        })
  }

  private case class QueryExecutionId(value: String) extends AnyVal

  private case class Query(value: String) extends AnyVal

  case class ProjectionClause(value: String) extends AnyVal

  case class RestrictionClause(value: String) extends AnyVal

  private val QueryStatusCheckDelayMillis = 200
}