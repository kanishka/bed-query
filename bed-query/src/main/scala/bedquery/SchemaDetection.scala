package bedquery

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Random, Success, Try}

import aws.MockGlueClient

object SchemaDetection {

  import MockGlueClient._

  /// Detect the applicable BED schema for the specified BED file. A table will be
  /// created in AWS Glue registry with the detected schema, under a newly created database.
  /// Currently BED files with 3, 4, 5, 6, 8, 9, or 12 columns are accepted.
  /// - bedFileS3Location: the location of the uploaded BED file to detect the schema on
  /// - glueClient: a connected AWS Glue client
  /// Returns: A Future, with a pair of the created database name and created table name,
  /// if successful
  private[bedquery] def detectRegisterTable(bedFileLocation: S3Location)(glueClient: MockGlueClient)(
    implicit ec: ExecutionContext): Future[(DatabaseName, TableName)] = {

    for {
      dbName <- createRandomDatabase(glueClient)
      _ <- createBedCrawler(BedCrawlerName, dbName, bedFileLocation)(glueClient)
      _ <- Future.fromTry(glueClient.startCrawler(BedCrawlerName.value))
      _ <- awaitCrawlerComplete(BedCrawlerName)(glueClient)
      tableNames <- Future.fromTry(glueClient.getTables(dbName.value).map(_.map(TableName(_))))
      _ <- if (tableNames.size == 1) Future.successful(()) else Future.failed(new Exception("Expected exactly 1 table"))
    } yield (dbName, tableNames.head)
    // TODO: Always delete the crawler if it was created
    // TODO: Delete the database if there was a failure
  }

  /// Create a temporary database for the detected schema table to be created within
  private def createRandomDatabase(glueClient: MockGlueClient)(implicit ec: ExecutionContext): Future[DatabaseName] = {

    val name = DatabaseName(new Random().alphanumeric.take(DatabaseNameLength).mkString)
    // TODO: check for name collision failure and retry with another random name, up to a retry limit
    Future.fromTry(glueClient.createDatabase(name.value)).map(_ => name)
  }

  /// Create a classifiers for each allowed BED format and create a crawler that makes use of them
  private def createBedCrawler(crawlerName: CrawlerName, dbName: DatabaseName, bedFileS3Location: S3Location)(glueClient: MockGlueClient)(
    implicit ec: ExecutionContext): Future[Unit] = {

    for {
      _ <- Future.sequence(
        BedGrokPatterns.map { case (name, pattern) =>
          Future.fromTry(glueClient.createGrokClassifier(name.value, pattern.value, UtilityPatterns))
        })
      _ <- Future.fromTry(glueClient.createCrawler(crawlerName.value, dbName.value, Seq(bedFileS3Location.value), BedGrokPatterns.keys.toSeq.map(_.value)))}
      yield ()
  }

  /// Await completion of a crawler
  private def awaitCrawlerComplete(crawlerName: CrawlerName)(glueClient: MockGlueClient)(implicit ec: ExecutionContext): Future[Unit] = {
    // Note: consider switching to using a Timer or investigating whether AWS async api actually supports non-blocking

    var result: Option[Try[Unit]] = None
    // TODO: take timeout parameter and compute timeRemaining
    var timeRemaining = true
    while (timeRemaining && result.isEmpty) {
      // TODO: ensure that this Ready is reported after completing the latest crawl by also inspecting last crawl information
      glueClient.getCrawler(crawlerName.value) match {
        case Success(Ready) => result = Some(Success(()))
        case Success(_) =>
        case failure => result = Some(failure.map(_ => ()))
      }
      Thread.sleep(CrawlerStatusCheckDelayMillis)
    }
    result match {
      case Some(value) => Future.fromTry(value)
      case None => Future.failed(new Exception("Timeout"))
    }
  }

  private case class CrawlerName(value: String) extends AnyVal

  private case class ClassifierName(value: String) extends AnyVal

  private case class GrokPattern(value: String) extends AnyVal

  private val BedCrawlerName = CrawlerName("bedCrawler")

  private val UtilityPatterns = Seq(
    "STRANDORIENT [+-]")

  // Note: one can use https://grokdebug.herokuapp.com/ to interactively test grok patterns
  // TODO: add type conversions to patterns for numeric fields
  // TODO: use custom patterns handle dot value for missing values
  private val BedGrokPatterns = {
    val bed3 = GrokPattern("%{WORD:chrom}%{SPACE}%{NONNEGINT:chromStart}%{SPACE}%{NONNEGINT:chromEnd}")
    val bed4 = GrokPattern(s"""${bed3.value}%{SPACE}%{WORD:name}""")
    val bed5 = GrokPattern(s"""${bed4.value}%{SPACE}%{NONNEGINT:score}""")
    val bed6 = GrokPattern(s"""${bed5.value}%{SPACE}%{STRANDORIENT:strand}""")
    val bed8 = GrokPattern(s"""${bed6.value}%{SPACE}%{NONNEGINT:thickStart}%{SPACE}%{NONNEGINT:thickEnd}""")
    val bed9 = GrokPattern(s"""${bed8.value}%{SPACE}%{NONNEGINT:r},%{NONNEGINT:g},%{NONNEGINT:b}""")
    val bed12 = GrokPattern(s"""${bed9.value}%{SPACE}%{NONNEGINT:blockCount}%{SPACE}%{NOTSPACE:blockSizes}%{SPACE}%{NOTSPACE:blockStarts}""")
    Map(ClassifierName("bed3") -> bed3,
      ClassifierName("bed4") -> bed4,
      ClassifierName("bed5") -> bed5,
      ClassifierName("bed6") -> bed6,
      ClassifierName("bed8") -> bed8,
      ClassifierName("bed9") -> bed9,
      ClassifierName("bed12") -> bed12)
  }

  private val CrawlerStatusCheckDelayMillis = 200

  private val DatabaseNameLength = 10
}