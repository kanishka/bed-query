package bedquery

import scala.concurrent.{ExecutionContext, Future}

import QueryExecution._
import SchemaDetection._
import aws._

/// Public API entry points for running ad-hoc queries against uploaded BED files
object AdHocQueryCoordinator {

  /// Detect the specific BED format and execute a SQL query against the contents.
  /// - bedFileLocation: the s3 location of the BED file
  /// - projections: the columns to return, some columns may have functions applied to them (the SQL Select clause)
  /// - restrictions: the filter criteria to use (the SQL Where clause)
  /// - outputFolder: the S3 folder that Athena will place query result intermediate files
  /// Returns: A future, with a sequence of rows, where each row consists of a sequence of string values
  /// Notes: The BED file must have comments stripped
  def registerAndRunQuery(
         bedFileLocation: S3Location,
         projections: ProjectionClause,
         restrictions: RestrictionClause,
         outputFolder: S3Location): Future[Seq[Seq[String]]] = {
    // TODO: have users and implementation reference column names using shared string constants

    implicit val ec = ExecutionContext.global
    for {(dbName, tableName) <- detectRegisterTable(bedFileLocation)(new MockGlueClient())
         rows <- executeQuery(projections, restrictions)(tableName, dbName, new MockAthenaClient(), outputFolder)
    } yield rows
  }

  // example usage:
  //   registerAndRunQuery(
  //      S3Location("s3://bucket1/folder1/file.bed"),
  //      ProjectionClause(" chromStart, chromEnd "),
  //      RestrictionClause(" chrom = 'chrom1' "),
  //      S3Location("s3://bucket1/folder1/result/"))

  // TODO: provide utility function to upload local BED file to s3 location
}
