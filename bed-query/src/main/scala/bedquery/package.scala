package object bedquery {
  case class S3Location(value: String) extends AnyVal
  private[bedquery] case class DatabaseName(value: String) extends AnyVal
  private[bedquery] case class TableName(value: String) extends AnyVal
}