package aws

import scala.util.{Success, Try}

object MockAthenaClient {

  sealed trait QueryExecutionState

  case object Cancelled extends QueryExecutionState

  case object Failed extends QueryExecutionState

  case object Succeeded extends QueryExecutionState

  case object Running extends QueryExecutionState

  case class GetQueryResultsResponse(columnInfo: Seq[Unit], rows: Seq[Seq[String]])
}

class MockAthenaClient() {
  import MockAthenaClient._

  def startQueryExecution(query: String, database: String, outputLocation: String): Try[String] = {
    Success("1")
  }

  def getQueryExecution(queryExecutionId: String): Try[QueryExecutionState] = {
    Success(Succeeded)
  }

  def getQueryResultsPaginator(queryExecutionId: String): Try[Iterable[GetQueryResultsResponse]] = {
    Success(Iterable())
  }
}
