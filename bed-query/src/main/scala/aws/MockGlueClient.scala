package aws

import scala.util.{Success, Try}

object MockGlueClient {

  sealed trait CrawlerState

  case object Ready extends CrawlerState

  case object Running extends CrawlerState

  case object Stopping extends CrawlerState

}

class MockGlueClient() {

  import MockGlueClient._

  def createCrawler(name: String, dbName: String, s3targets: Seq[String], customClassifiers: Seq[String]): Try[Unit] = {
    Success(())
  }

  def createGrokClassifier(name: String, grokPattern: String, customPatterns: Seq[String]): Try[Unit] = {
    Success(())
  }

  def startCrawler(name: String): Try[Unit] = {
    Success(())
  }

  def getCrawler(name: String): Try[CrawlerState] = {
    Success(Ready)
  }

  def getTables(dbName: String): Try[Seq[String]] = {
    Success(Seq("table1"))
  }

  def createDatabase(name: String): Try[Unit] = {
    Success(())
  }
}
